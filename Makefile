# Environment variables -------------------------------------------------------
SHELL:=/bin/bash
PHP?=$(shell command -v php)
COMPOSER?=composer.phar

# Default target --------------------------------------------------------------
.PHONY: install
install: composer_install

# Composer rules --------------------------------------------------------------
.PHONY: composer_install composer_update composer_selfupdate

composer_install:
	$(PHP) $(COMPOSER) install --prefer-dist

composer_update:
	$(PHP) $(COMPOSER) update --prefer-dist

composer_selfupdate:
	$(PHP) $(COMPOSER) self-update
