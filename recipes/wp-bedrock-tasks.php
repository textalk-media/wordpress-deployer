<?php

/**
 * Tasks and settings used for deploying a WordPress project based on Bedrock.
 */

namespace Deployer;

/*
 * Configuration that should be similar to all projects.
 *
 * Note that the wp-config.php can not be set in shared_files, since symlinking it
 * will break WordPress (confirmed in version 4.6.1). Instead, this file is copied
 * to the release root folder.
 */

/*
 * build_path is the relative or absolute path to the release folder being built.
 */
set('build_path', 'build');

/*
 * remote_transfer_method (ssh|scp|deployer)
 */
set('remote_transfer_method', 'ssh');

set('bin/localphp', function () {
    return runLocally('which php');
});

/*
 * Overwrite the composer binary fetching, since the one inherited from Deployer
 * requires the release_path to be set, which it wouldn't be on locally build.
 * Download composer.phar on the fly if needed.
 */
set('bin/composer', function () {
    if (testLocally('hash composer 2>/dev/null')) {
        $composer = runLocally('which composer || true');
    }

    if (empty($composer)) {
        $composer = get('build_path') . '/composer.phar';
        if (!is_readable($composer)) {
            $pwd = runLocally('pwd');
            runLocally("cd {{build_path}} && curl -sS https://getcomposer.org/installer | {{bin/localphp}}");
            $composer = "{{bin/localphp}} $pwd/{{build_path}}/composer.phar";
        }
    }
    return $composer;
});


/**
 * Prepare for building the project locally.
 */
task('deploy:bedrock:prepare', function () {
    $build_path = get('build_path');
    if (empty($build_path)) {
        $formatter = Deployer::get()->getHelper('formatter');
        $errorMessage = ["Setting for build directory is invalid."];
        write($formatter->formatBlock($errorMessage, 'error', true));
        throw new \RuntimeException($errorMessage);
    }

    // Delete the old build directory and create a new one.
    runLocally('rm -rf ' . escapeshellarg($build_path));
    runLocally('mkdir -p ' . escapeshellarg($build_path));
});

/*
 * Fetch the git repo and store it in locally in the build directory.
 */
task('deploy:bedrock:update_code', function () {
    writeln(sprintf(
        'Fetch branch <info>%s</info> from <info>%s</info>',
        get('branch'),
        get('repository')
    ));

    // Run git archive and unpack in build directory.
    runLocally(
        'git ' . implode(' ', [
            'archive',
            '--remote',
            escapeshellarg(get('repository')),
            escapeshellarg(get('branch')),
        ]) . ' | '

        . 'tar -x -C ' . escapeshellarg(get('build_path')),
        ['timeout' => 300]
    );
});

/*
 * Get the commit hash to be deployed and save it in the build directory in the file "version.txt".
 */
task('deploy:bedrock:write_commit_hash', function () {
    writeln(sprintf(
        'Write commit hash to <info>%s</info>.',
        'version.txt'
    ));

    // Run git archive and unpack in build directory.
    runLocally(
        'git ' . implode(' ', [
            'archive',
            '--remote',
            escapeshellarg(get('repository')),
            escapeshellarg(get('branch')),
        ]) . ' | '
        . 'git get-tar-commit-id > ' . escapeshellarg(get('build_path') . '/' . 'version.txt'),
        ['timeout' => 300]
    );
});


/*
 * Fetch dependencies (i.e. composer for now).
 */
task('deploy:bedrock:vendors', function () {
    runLocally('cd {{build_path}} && {{bin/composer}} {{composer_options}}', ['timeout' => 300]);
});


/**
 * Delete files and folders from the built directory that doesn't need to be published.
 */
task('deploy:bedrock:delete_surplus_files', function () {
    runLocally('cd {{build_path}} && rm -f .git*');
    runLocally('cd {{build_path}} && rm -f .env.example');
    runLocally('cd {{build_path}} && rm -f deploy.php');
    runLocally('cd {{build_path}} && rm composer.*');
});


/**
 * Override in the project deploy configuration to build project specific things.
 */
task('deploy:bedrock:build_custom', function () {
});


/**
 * Cleanup locally after a successful deployment.
 */
task('deploy:bedrock:cleanup', function () {
});


/**
 * Upload the locally built release via ssh.
 */
task('deploy:bedrock:upload', function () {
    // Local servers always use copy.
    $host = \Deployer\Task\Context::get()->getHost();
    $is_local = ($host instanceof \Deployer\Host\Localhost);

    if ($is_local) {
        // The upload method doesn't work on directories in Deployer's Local implementation... :/
        // find is used for hidde nfiles to copy correctly.
        writeLn("Copy the files since remote is the local machine.");
        runLocally(
            'find {{build_path}} -mindepth 1 -maxdepth 1 -exec cp -r {} '
            . escapeshellarg(get('release_path') . '/') . ' \;',
            ['timeout' => 300]
        );
    } else {
        /*
         * There are basically three ways to transfer the build folder to the server via ssh,
         * which is set in the setting "ssh_transfer_method". We should consider implementing
         * rsync which is probably the best way of uploading if both servers support it.
         *
         * 1. ssh_transfer_method = "deployer" uses Deployer's built in upload method. Since
         *    it creates one scp command for each file,it is very time-consuming for a large
         *    amount of files.
         *    The advantage is that we use Deployer and do not have to parse settings etc.
         *
         * 2. Implementing scp with the -r flag to copy files recursively.
         *    This is much faster than Deployer's built in transfer.
         *
         * 3. Archive the files into a tar and send it via ssh, and let the server unpack and
         *    save the files remotely.
         *    This is the preferred and default solution since it it the fastest one.
         */
        switch (get('remote_transfer_method')) {

            case 'deployer':
                writeLn("Using file-by-file scp to upload files (Deployer's internal upload function.");
                foreach (scandir(get('build_path')) as $file) {
                    $file_full = get('build_path') . '/' . $file;
                    if ($file === '.' || $file === '..') {
                        continue;
                    }
                    upload('{{build_path}}/' . $file, get('release_path'));
                }
                break;

            case 'scp':
                // Deployer's own functionality doesn't allow to scp directories recursively,
                // so building our own scp implementation using the -r flag will be faster.
                writeLn("Using recursive scp to upload files.");

                $args = ['-r'];
                if (isDebug()) {
                    $args[] = '-v';
                }
                if ($host->getPort()) {
                    $args[] = '-P';
                    $args[] = $host->getPort();
                }
                if ($host->getIdentityFile()) {
                    $args[] = '-i';
                    $args[] = $config->getIdentityFile();
                }
                $args[] = './';

                $args[] = ((string) $host) . ':' . get('release_path') . '/';

                runLocally(
                    'cd {{build_path}} && scp ' . implode(' ', array_map('escapeshellarg', $args)),
                    ['timeout' => 300]
                );
                break;

            case 'ssh':
                writeLn("Using ssh/tarball to upload files.");

                $args = [];
                if (isDebug()) {
                    $args[] = '-v';
                }
                if ($host->getPort()) {
                    $args[] = '-p';
                    $args[] = $host->getPort();
                }
                if ($host->getIdentityFile()) {
                    $args[] = '-i';
                    $args[] = $host->getIdentityFile();
                }

                // Stringifying the host will prefix with the user followed by @, if any.
                $args[] = (string) $host;

                $remote_command = 'cd ' . escapeshellarg(get('release_path')) . ' && tar -xf -';
                $ssh_command = 'ssh ' . implode(' ', array_map('escapeshellarg', $args)) . ' "' . $remote_command . '"';

                runLocally(
                    'cd {{build_path}} && tar -cf - . | ' . $ssh_command,
                    ['timeout' => 300]
                );
                break;
            default:
                throw new \LogicException("Invalid setting for remote_transfer_method");
        }
    }
});
