<?php

/**
 * Basic configuration settings for deploying WordPress projects that use
 * composer for dependency handling.
 *
 * The project including this recipe must include Deployer's common recipe before
 * including this file, to ensure that default settings are initially set.
 */

namespace Deployer;

/*
 * Include all default settings and tasks that handle building the release locally.
 */
require __DIR__ . '/wp-localbuild-tasks.php';

/*
 * Configuration that should be similar to all projects.
 *
 * Note that the wp-config.php can not be set in shared_files, since symlinking it
 * will break WordPress (confirmed in version 4.6.1). Instead, this file is copied
 * to the release root folder.
 */
set('ssh_type', 'native');
set('shared_files', ['www/.htaccess']);
set('shared_dirs', ['www/wp-content/uploads', 'www/wp-content/blogs.dir']);

/*
 * These can be overridden to copy in certain plugins or themes when building.
 */
set('extra_plugins_dir', '');
set('extra_themes_dir', '');

/*
 * Be expressive in the terminate message.
 */
task('success', function () {
    writeln(sprintf(
        '<info>Successfully deployed to %s</info>',
        substr(get('release_path'), 1 + strlen(get('deploy_path')))
    ));
});

/*
 * The deploy task.
 */
task('deploy', [
    'deploy:info',
    'deploy:localbuild:prepare',
    'deploy:localbuild:update_code',
    'deploy:localbuild:vendors',
    'deploy:localbuild:replace_symlinks',
    'deploy:localbuild:install_extras',
    'deploy:localbuild:delete_surplus_files',
    'deploy:localbuild:build_custom',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:localbuild:upload',
    'deploy:localbuild:env_files',
    'deploy:shared',
    'deploy:symlink',
    'deploy:unlock',
    'deploy:localbuild:cleanup',
    'cleanup',
])->desc('Build locally and deploy with SSH');

after('deploy', 'success');
