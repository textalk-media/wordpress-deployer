<?php

/**
 * Basic configuration settings for deploying WordPress projects that use
 * composer for dependency handling.
 *
 * The project including this recipe must include Deployer's common recipe before
 * including this file, to ensure that default settings are initially set.
 */

namespace Deployer;

/*
 * Configuration that should be similar to all projects.
 *
 * Note that the wp-config.php can not be set in shared_files, since symlinking it
 * will break WordPress (confirmed in version 4.6.1). Instead, this file is copied
 * to the release root folder.
 */
set('ssh_type', 'native');
set('shared_files', ['www/.htaccess']);
set('shared_dirs', ['wp-content/uploads', 'wp-content/blogs.dir']);

/**
 * Override in the project deploy configuration to build project specific things.
 */
task('deploy:wp_build', function () {
});

task('deploy:wp_prepare', function () {
    /*
     * Ensure that plugins and themes folders exist if not checked in.
     */
    cd('{{release_path}}');
    writeln("Create themes and plugins folders if needed");
    run('mkdir -p wp-content/themes wp-content/plugins');
});

/**
 * Move in non-composer themes and/or themes.
 */
task('deploy:move_extra_themes_and_plugins', function () {
    cd('{{release_path}}');
    if (get('extra_themes_dir')) {
        writeln("Copy non-composer themes into <info>wp-content/themes/</info>");
        run('cp -a extra-themes/* wp-content/themes/');
    }
    if (get('extra_plugins_dir')) {
        writeln("Copy non-composer plugins into <info>wp-content/plugins/</info>");
        run('cp -a extra-plugins/* wp-content/plugins/');
    }
});

/**
 * Cleanup things on server that were in repo but is not needed there.
 */
task('deploy:wp_cleanup', function () {
    cd('{{release_path}}');
    if (get('extra_themes_dir')) {
        writeln(sprintf(
            'Delete checked-in non-composer themes from <info>%s/</info>',
            get('extra_themes_dir')
        ));
        run('rm -rf ' . escapeshellarg(get('extra_themes_dir')));
    }
    if (get('extra_plugins_dir')) {
        writeln(sprintf(
            'Delete checked-in non-composer plugins from <info>%s/</info>',
            get('extra_plugins_dir')
        ));
        run('rm -rf ' . escapeshellarg(get('extra_plugins_dir')));
    }
});

/**
 * Move the wp-config.php file from shared area into the release root folder,
 * if previous steps did not cover that already.
 */
task('deploy:wp_install_config_file', function () {
    cd('{{release_path}}');
    run('cp -n {{deploy_path}}/shared/wp-config.php {{release_path}}/');
});

/*
 * Be expressive in the terminate message.
 */
task('success', function () {
    writeln(sprintf(
        '<info>Successfully deployed to %s</info>',
        substr(get('release_path'), 1 + strlen(get('deploy_path')))
    ));
});


/*
 * The deploy task.
 */
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:vendors',
    'deploy:shared',
    'deploy:wp_prepare',
    'deploy:move_extra_themes_and_plugins',
    'deploy:build_wp',
    'deploy:wp_install_config_file',
    'deploy:wp_cleanup',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy your WordPress project');

after('deploy', 'success');
