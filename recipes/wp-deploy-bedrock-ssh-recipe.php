<?php

/**
 * Basic configuration settings for deploying a WordPress project set up in Bedrock.
 *
 * The project including this recipe must include Deployer's common recipe before
 * including this file, to ensure that default settings are initially set.
 */

namespace Deployer;

/*
 * Include all default settings and tasks that handle building a Bedrock project.
 */
require __DIR__ . '/wp-bedrock-tasks.php';

/*
 * Configuration that should be similar to all projects.
 */
set('ssh_type', 'native');
set('shared_files', ['.env']);
set('shared_dirs', ['web/app/uploads']);

/*
 * Be expressive in the terminate message.
 */
task('success', function () {
    writeln(sprintf(
        '<info>Successfully deployed to %s</info>',
        substr(get('release_path'), 1 + strlen(get('deploy_path')))
    ));
});

/*
 * The deploy task.
 */
task('deploy', [
    'deploy:info',
    'deploy:bedrock:prepare',
    'deploy:bedrock:update_code',
    'deploy:bedrock:write_commit_hash',
    'deploy:bedrock:vendors',
    'deploy:bedrock:delete_surplus_files',
    'deploy:bedrock:build_custom',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:bedrock:upload',
    'deploy:shared',
    'deploy:symlink',
    'deploy:unlock',
    'deploy:bedrock:cleanup',
    'cleanup',
])->desc('Build locally and deploy with SSH');

after('deploy', 'success');
