# WordPress Deployer

This software provides a simple way to setup deployment for a WordPress project stored in git. It wraps the deployer/deployer project and provides a recipe that serves as a default configuration for any WordPress projects based on git and preferably composer.

Using WordPress Deployer for deployment requires:
  - A local Linux/Unix environment where deployment is triggered, having php command line support
  - The local environment to have access to a git server hosting the project
  - One or more remote or local Linux/Unix environments where project is deployed to
  - Local and remote ssh support if deploying remotely
  - A WordPress project repo that uses composer to install WordPress Deployer

## Deplying with deployer/deployer

Deployment process wraps the deployer/deployer project found at https://deployer.org where also documentation is found.

In short, deployment checks out the project repository and executes necessary build steps. The project is deployed in a separate release folder, and a symlink is pointed at the deployed version if the build process finishes correctly. The release change is made atomically and this makes it possible to rollback to an older version atomically.

The following folders will be created on the deployment destination:
  - releases/NNN (contains the built project, where NNN is an integer naming each deployed release in numerical order)
  - shared (contains files shared between releases such as uploads and config files)
  - current (a symlink to the currently active release)
  - .dep (hidden folder containing internal information, e.g. when releases were added)

The web server should point to the folder _current/www_ or another folder within each release that contains the WordPress _index.php_ file.

## Installation

First, make sure that your development environment has access to this repository. Then, include this project in your WordPress project _composer.json_ file, by adding it manually or entering:

```sh
$ bin/composer.phar require textalk/wp-deployer
```

Copy one of the template configuration files to your project root and name it _deploy.php_. Modify the file to match your project.


## Setup

### Shared files

#### Upload folders
When deploying the first time, the upload folder will be moved or created in _shared/wp-content/upload_. The deployer will create a symlink to the upload area for each new release being deployed. Similarly, the _blogs.dir_ folder will be created/symlinked, since it is used in some multisite WordPress installations.

#### WordPress configuration file

Since passwords should normally not be checked in, the deployer assumes that a _wp-config.php_ file is placed in the _shared_ folder. This real config file is copied to the release root folder to force WordPress to recognize it (symlinking does not work). WordPress supports placing the _wp-config.php_ file in the parent folder of the folder containing the WordPress files, which is better for security reasons.

### Recipes

The recipes that come with this project defines what tasks that deployment should include, and adds some tasks that will be executed in the deployment process. It is possible, but seldom needed, to override tasks defined in these recipes.

### Project specific configuraion

The project should create a deploy.php configuration file in the project root. This file should contain:
  - Project specific settings
  - Custom project specific build steps
  - Configuration of the servers being deployed to

This project contains a template file that serves as a starting point for this file. See above in chapter *Installation*.

#### Project specific settings

Replace the placeholder values for git url and branch.

If your project uses themes that are not fetched via composer, place them in a folder and set the folder name in the _extra_themes_dirname_ setting. Similarly for such plugins. This is a way of keeping such code checked-in in our repo.

#### Custom build steps

Put your project specific build steps in a task called *deploy:build_wp* in your project configuration file.

#### Server configuration

Servers are defined as described in the deployer/deployer documentation.

It is possible to define multiple destinations with the `host()` function, according to the Deployer documentation.

Examples are provided in the [template file](templates/deploy.php).

## Usage

### Deploying

Deplying is made by executing the provided _dep_ script together with the task and server label (as defined in the configuration file). For example:
```sh
$ vendor/bin/dep deploy production
```

The deployment will add a lock file remotely, which will be removed when deployment has finished successfully. If something breaks under deployment, you must unlock with:
```sh
$ vendor/bin/dep deploy:unlock production
```

### Rolling back

Rolling back to the previous version is done by running the _rollback_ task:

```sh
$ vendor/bin/dep rollback production
```

## License
----
Proprietary, © Textalk AB, Sweden
