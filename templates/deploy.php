<?php

/**
 * Template configuration file for WordPress project.
 * The default location for this file is in the git repository root.
 *
 * Edit this file to fit specific needs for your WordPress deployment.
 * See https://deployer.org/docs for configuration options.
 *
 * This configuration requires textalk/wp-deployer to be installed with composer.
 */

namespace Deployer;

/*
 * Load defaults from the common recipe in the Deployer library.
 */
require __DIR__ . '/vendor/deployer/deployer/recipe/common.php';

/*
 * Include the WordPress recipe for this type of WP installation.
 */
require __DIR__ . '/vendor/textalk/wp-deployer/recipes/wp-deploy-localbuild-ssh-recipe.php';

/*
 * Set the git repository and branch.
 */
set('repository', '<YOUR GIT REPOSITORY REFERENCE>');
set('branch', '<YOUR GIT BRANCH USED WHEN DEPLOYING>');

/*
 * Tell Deployer that we should copy themes and/or plugins from these folders.
 * Content within these directories will be moved into WordPress themes and
 * plugins directory. This is useful if using themes or repositories that are
 * not installed via Composer.
 */
set('extra_themes_dir', 'extra-themes');
set('extra_plugins_dir', 'extra-plugins');

/*
 * Build WordPress specifically for this project.
 */
task('deploy:build_wp', function () {
    // Feel free to place custom code here that builds your project.
});


// Configure all servers here.

/*
 * Example of a local demo server setup, that needs no authentication.
 */
host('my-production-domain.com')
    ->stage('demo')
    ->set('deploy_path', '/www/my-local-demo-domain.com')
;

/*
 * Example of a production server setup, where the current user's password is requested
 * and entered manually at deployment.
 */
host('my-production-domain.com')
    ->stage('demo')
    ->user(posix_getpwuid(posix_geteuid())['name'])
    ->password(null)
    ->set('deploy_path', '/some/path/to-the-project')
;

/*
 * Example of a production server setup, where an SSH key is used to deploy as the
 * specified remote user.
 */
host('99.99.99.99')
    ->stage('production')
    ->user('<REMOTE USERNAME>')
    ->set('deploy_path', '/some/other/path/my-production-domain.com')
;
